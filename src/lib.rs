//! A simple example password manager in Rust.
//!
//! No effort to thwart timing attacks, other side-channel attacks, and other
//! real world concerns. This library is only an example of the basic principles
//! and not meant for any real world use.
//!
//! DO NOT USE THIS LIBRARY FOR ANYTHING THAT NEEDS TO BE SECURE.
//!
//! It is just a prototype to test out some rust libraries for later use.
//!
//! ----------------------------------------------------------------------------
//!
//! In order to accommodate a potentially large number of entries in the
//! password  manager, it will encrypts and stores each record individually
//! in-memory. This way, it is not necissary to have to decrypt every entry
//! in the password manager when fetching a single record.

extern crate rust_sodium;

use std::error::Error;
use std::fmt;

/// The keychain.
pub trait Keychain {
    /// Create a new Key Value Store (KVS).
    ///
    /// Generates the necessary keys needed to provide for the various
    /// functionalities of the password manager. Once initialized the
    /// password manager is ready to support all it's functionality.
    ///
    /// Run-time: O(1)
    fn init(&self, password: String);

    /// Load the keychain state from a serialized representation.
    ///
    /// Assumes that `representation` is a valid serialization generated
    /// by a call to `keychain.dump()`. Verifies that the given password
    /// is valid for the keychain. If the parameter `trusted_data_check`
    /// is provided, this function also affirms the integrity of the Key
    /// Value Store (KVS). If tampering is detected the function throws
    /// an exception. If everything passes, the function returns `true`
    /// and the keychain object is ready to support all other functionality
    /// described in the API. If the provided password is invalid, the
    /// function returns false.
    ///
    /// Runtime: O(n)
    fn load(&self, password: String, representation: String) -> Result<(), KeychainError>;

    /// Return a Bincode encoded serialization of the keychain, and its SHA-256 hash.
    ///
    /// If the keychain has not been initialized or successfully loaded
    /// into memory, this method returns `Err`. Otherwise it creates a
    /// serialization of the keychain, such that it may be loaded back
    /// into memory via a subsequent call to `keychain.load`. It returns
    /// an array consisting of this, and also the SHA-256 hash of the
    /// keychain contents (which is stored in trusted sotrage, and used
    /// to prevent rollback attacks).
    ///
    /// Runtime O(n)
    fn dump(&self) -> Result<([u8;32], String), KeychainError>;

    /// Insert the domain and associated data into the KVS.
    ///
    /// If the keychain has not been initialized or successfully loaded
    /// into memory, this method throws an exception: 
    /// `throw "Keychain not initialized."` Otherwise, the method inserts
    /// the domain and associated data into the KVS. If the domain is
    /// already in the password manager, this method will update its value.
    /// Otherwise, it will create a new entry.
    ///
    /// Runtime O(n)
    fn set(&self, name: [u8], value: [u8]) -> Result<(), KeychainError>;

    /// Returns the the saved data associated with the domain.
    ///
    /// If the keychain has not been initialized or successfully loaded
    /// into memory, this method should throw an exception:
    /// `throw "Keychain not initialized."` If the requested domain is
    /// in the KVS, then this method should return the the saved data
    /// associated with the domain. If the requested domain is not in
    /// the KVS, then this method should return `Err`.
    ///
    /// Runtime O(n)
    fn get(&self, name: [u8]) -> Result<(), KeychainError>;

    /// Remove the record from the KVS.
    ///
    /// If the keychain has not been initialized or successfully loaded
    /// into memory, this method should throw an exception:
    /// `throw "Keychain not initialized."` If the requested domain is
    /// in the KVS, then this method should remove the record from the
    /// KVS. The method returns `Ok` in this case. Otherwise, if the
    /// specified domain is not present, return `Err`.
    ///
    /// Runtime O(n)
    fn remove(&self, name: [u8]) -> Result<(), KeychainError>;
}

#[derive(Debug)]
pub enum KeychainError {
    InvalidPassword(InvalidPasswordError),
    KeychainIntegrity(KeychainIntegrityError),
    NotInitialized(KeychainNotInitializedError),
    DomainNotAvailable(DomainNotAvailableError),
}

impl fmt::Display for KeychainError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Keychain Error")
    }
}

impl Error for KeychainError {
    fn description(&self) -> &str {
        match *self {
            KeychainError::InvalidPassword(ref err) => err.description(),
            KeychainError::KeychainIntegrity(ref err) => err.description(),
            KeychainError::NotInitialized(ref err) => err.description(),
            KeychainError::DomainNotAvailable(ref err) => err.description(),
        }
    }

    fn cause(&self) -> Option<&Error> {
        match *self {
            KeychainError::InvalidPassword(ref err) => Some(err),
            KeychainError::KeychainIntegrity(ref err) => Some(err),
            KeychainError::NotInitialized(ref err) => Some(err),
            KeychainError::DomainNotAvailable(ref err) => Some(err),
        }
    }
}

#[derive(Debug)]
pub struct InvalidPasswordError;

impl fmt::Display for InvalidPasswordError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid password")
    }
}

impl Error for InvalidPasswordError {
    fn description(&self) -> &str {
        "Invalid password"
    }
}

#[derive(Debug)]
pub struct KeychainIntegrityError;

impl fmt::Display for KeychainIntegrityError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Keychain integrity failure")
    }
}

impl Error for KeychainIntegrityError {
    fn description(&self) -> &str {
        "Keychain integrity check failed"
    }
}

#[derive(Debug)]
pub struct KeychainNotInitializedError;

impl fmt::Display for KeychainNotInitializedError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Keychain not initialized")
    }
}

impl Error for KeychainNotInitializedError {
    fn description(&self) -> &str {
        "Keychain not initialized"
    }
}

#[derive(Debug)]
pub struct DomainNotAvailableError;

impl fmt::Display for DomainNotAvailableError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Domain not available")
    }
}

impl Error for DomainNotAvailableError {
    fn description(&self) -> &str {
        "Domain not available in the keychain"
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
