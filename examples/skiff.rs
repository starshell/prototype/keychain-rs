//! A simple example password manager in Rust.
//!
//! No effort to thwart timing attacks, other side-channel attacks, and other
//! real world concerns. This library is only an example of the basic principles
//! and not meant for any real world use.
//!
//! DO NOT USE THIS LIBRARY FOR ANYTHING THAT NEEDS TO BE SECURE.
//!
//! It is just a prototype to test out some rust libraries for later use.
//!
//! ----------------------------------------------------------------------------
//!
//! In order to accommodate a potentially large number of entries in the
//! password  manager, it will encrypts and stores each record individually
//! in-memory. This way, it is not necissary to have to decrypt every entry
//! in the password manager when fetching a single record.

#[macro_use]
extern crate serde_derive;

extern crate serde;
extern crate keyring;
extern crate rust_sodium;
extern crate bincode;


//use std::collections::HashMap;

use keyring::*;
use rust_sodium::crypto::pwhash;
use bincode::{serialize, deserialize};

#[derive(Serialize, Deserialize, Debug)]
struct Skiff {
    initialized: bool,
    password_hash_bytes: [u8; pwhash::HASHEDPASSWORDBYTES],
    //key_value_store: HashMap,
}

impl Keychain for Skiff {
    fn init(&self, password: String) {
        rust_sodium::init();
        let password_hash = pwhash::pwhash(
            password,
            pwhash::OPSLIMIT_INTERACTIVE,
            pwhash::MEMLIMIT_INTERACTIVE,
        ).unwrap();
        self.password_hash_bytes = &password_hash[..];
        self.initialized = true;
    }

    fn load(&self, password: String, representation: String) -> Result<(), KeychainError> {
        rust_sodium::init();
        if !self.initialized {
            return Err(KeychainNotInitializedError);
        }
        if !pwhash::pwhash_verify(&self.password_hash_bytes, password) {
            return Err(InvalidPasswordError);
        }
        self = deserialize(&representation[..]).unwrap();
        Ok(())
    }

    fn dump(&self) -> Result<([u8; 32], String), KeychainError> {}
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
